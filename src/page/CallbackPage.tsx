import { useEffect } from "react";
import { supabase } from "../api";
import { App, toastErrorHandler } from "../App";
import { NonIdealState, Spinner, SpinnerSize } from "@blueprintjs/core";
import { useNavigate } from "react-router-dom";

export default function CallbackPage() {
  const navigate = useNavigate();
  useEffect(() => {
    const params = new URLSearchParams(window.location.hash.split("#", 3)[2]);

    (async () => {
      try {
        await supabase.auth.setSession({
          access_token: params.get("access_token")!,
          refresh_token: params.get("refresh_token")!,
        });
        navigate("/reauth");
      } catch (error) {
        toastErrorHandler(error);
      }
    })();
  }, [navigate]);

  return App(<NonIdealState icon={<Spinner size={SpinnerSize.LARGE} />} />);
}
