export function titleCase(s: string): string {
  return s
    .split("-")
    .map((i) => `${i.at(0)?.toUpperCase()}${i.substring(1)}`)
    .join(" ");
}

export function delay(t: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, t));
}

export function classes(names: Record<string, boolean>): string {
  const classList = [];
  for (const key in names) {
    if (Object.prototype.hasOwnProperty.call(names, key)) {
      if (names[key]) classList.push(key);
    }
  }

  return classList.join(" ");
}
