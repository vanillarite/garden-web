import { useNavigate, useParams } from "react-router-dom";
import { ApiCard, ApiCardResponse, ApiColumn, apiFetch, ID, useUser } from "../api";
import { createContext, useCallback, useContext, useEffect, useState } from "react";
import {
  Alert,
  Button,
  Callout,
  Classes,
  Code,
  Dialog,
  DialogBody,
  Divider,
  EditableText,
  EntityTitle,
  H6,
  IconName,
  Intent,
  Popover,
  Spinner,
  SpinnerSize,
  TextArea,
} from "@blueprintjs/core";
import { toastErrorHandler } from "../App";
import { BoardContext } from "../page/BoardPage";
import { convertTagsToCardTagElements, TagSelectorContainer } from "./tagComponents";
import Markdown from "react-markdown";
import { Avatar } from "./boardComponents";
import { classes } from "../util";

function DescriptionEditor({
  initialContent,
  onSave,
  onCancel,
  saveText,
  disabled,
}: {
  initialContent: string;
  onSave: (content: string) => void;
  onCancel: () => void;
  saveText: string;
  disabled: boolean;
}) {
  const [content, setContent] = useState(initialContent);
  return (
    <div
      className="fill-width"
      // onBlur={(e) => {
      //   if (e.relatedTarget !== e.currentTarget && !e.currentTarget.contains(e.relatedTarget)) {
      //     onSave(content);
      //   }
      // }}
    >
      <TextArea
        autoResize={true}
        className="andika"
        defaultValue={initialContent}
        onChange={(e) => setContent(e.currentTarget.value)}
        fill={true}
        style={{ resize: "none" }}
        autoFocus={true}
        disabled={disabled}
      />
      <Button intent={Intent.SUCCESS} text={saveText} onClick={() => onSave(content.trim())} />
      <Button intent={Intent.DANGER} text="Cancel" onClick={() => onCancel()} />
    </div>
  );
}

interface CardContextType {
  card?: ApiCardResponse;
  id: ID<"c">;
  tags: readonly string[];
  refetchCard: () => Promise<void>;
  setCard: (board: ApiCardResponse) => void;
  setPopupOpen: (open: boolean) => void;
}
export const CardContext = createContext<CardContextType>(null!); // Meta thinks they know better than developers

function PopupSection({
  title,
  icon,
  inline,
  children,
}: {
  title: string;
  icon: IconName;
  inline?: JSX.Element;
  children?: React.ReactNode[] | React.ReactNode;
}) {
  return (
    <>
      <div
        className={classes({
          "card-popup-section": true,
          inline: inline !== undefined,
          hybrid: inline !== undefined && children !== undefined,
        })}
      >
        <EntityTitle title={title} heading={H6} icon={icon} className="heading" />
        {inline && <div className="value-container">{inline}</div>}
      </div>
      {children && <div>{children}</div>}
    </>
  );
}

function PopupContent() {
  const [isTagPanelOpen, setTagPanelOpen] = useState(false);

  const [isEditingDescription, setEditingDescription] = useState(false);
  const [isSubmittingDescription, setSubmittingDescription] = useState(false);

  // TODO: deduplicate?
  const [isEditingComment, setEditingComment] = useState(false);
  const [isSubmittingComment, setSubmittingComment] = useState(false);

  const boardContext = useContext(BoardContext);
  const cardContext = useContext(CardContext);
  const card = cardContext.card!;
  const board = boardContext.board;
  const tags = boardContext.tags;
  const me = useUser();

  const description = card.desc?.description ?? "";

  const postDescription = async (content: string) => {
    setSubmittingDescription(true);
    if (content !== description) {
      try {
        await apiFetch<void>(`/board/${boardContext.id}/card/${card.card.id}/description`, "POST", {
          description: content,
        });
        cardContext.refetchCard();
        boardContext.refetchBoard(); // i don't like this, this is just to display the "tag" below the title
      } catch (error) {
        toastErrorHandler(error);
      }
    }
    setSubmittingDescription(false);
    setEditingDescription(false);
  };

  const postComment = async (content: string) => {
    setSubmittingComment(true);
    if (content !== "") {
      try {
        await apiFetch<void>(`/board/${boardContext.id}/card/${card.card.id}/comment`, "POST", { body: content });
        cardContext.refetchCard();
        boardContext.refetchBoard(); // i don't like this, this is just to display the "tag" below the title
      } catch (error) {
        toastErrorHandler(error);
      }
    }
    setSubmittingComment(false);
    setEditingComment(false);
  };

  const [deleteAlertOpen, setDeleteAlertOpen] = useState(false);
  const [deleteAlertLoading, setDeleteAlertLoading] = useState(false);
  const deleteCard = async () => {
    setDeleteAlertLoading(true);
    try {
      await apiFetch<void>(`/board/${boardContext.id}/card/${card.card.id}`, "DELETE");
      boardContext.refetchBoard();
      setDeleteAlertOpen(false);
      cardContext.setPopupOpen(false);
    } catch (error) {
      toastErrorHandler(error);
    }
    setDeleteAlertLoading(false);
  };

  return (
    <>
      <PopupSection
        title="Tags"
        icon="tag"
        inline={
          <>
            <div className="value-container">
              <Popover
                isOpen={isTagPanelOpen}
                onClose={() => setTagPanelOpen(false)}
                position={"left-top"}
                content={<TagSelectorContainer />}
              >
                {card.card.tags === "" ? (
                  <Button icon="insert" text="No tags. Click to add" onClick={() => setTagPanelOpen(true)} />
                ) : (
                  <Button icon="edit" onClick={() => setTagPanelOpen(true)} />
                )}
              </Popover>
              {convertTagsToCardTagElements(card.card.tags, tags, {
                large: true,
                onClick: () => setTagPanelOpen(true),
              })}
            </div>
          </>
        }
      />

      <PopupSection
        title="Description"
        icon="align-left"
        inline={
          <>
            {description === "" && !isEditingDescription && (
              <Button icon="insert" text="No description. Click to add" onClick={() => setEditingDescription(true)} />
            )}
            {description !== "" && !isEditingDescription && (
              <Button icon="edit" onClick={() => setEditingDescription(true)} />
            )}
          </>
        }
      >
        {(description !== "" || isEditingDescription) && (
          <div>
            {isEditingDescription ? (
              <DescriptionEditor
                initialContent={description}
                onSave={postDescription}
                onCancel={() => setEditingDescription(false)}
                saveText="Save"
                disabled={isSubmittingDescription}
              />
            ) : (
              <div
                onClick={() => setEditingDescription(true)}
                className={isSubmittingDescription ? "desc-p wait" : "desc-p pointer"}
              >
                <Markdown>{description}</Markdown>
              </div>
            )}
          </div>
        )}
      </PopupSection>

      <PopupSection title="Comments" icon="comment">
        <div className="comment-area">
          <Avatar user={me} />
          {!isEditingComment && (
            <Button icon="insert" text="Click to add a comment" onClick={() => setEditingComment(true)} />
          )}
          {isEditingComment && (
            <DescriptionEditor
              initialContent={""}
              onSave={postComment}
              onCancel={() => setEditingComment(false)}
              saveText="Post"
              disabled={isSubmittingComment}
            />
          )}
        </div>
        {card.comments.length > 0 && <Divider />}
        {[...card.comments].reverse().map((comment) => {
          const user = board?.members.find((user) => user.id === comment.user_id);
          const time = new Date(Date.parse(comment.created_at));
          return (
            <div key={comment.id} className="comment-area">
              <Avatar user={user} />
              <div>
                <div className="comment-title">
                  <b className={classes({ [Classes.SKELETON]: user === undefined, name: true })}>
                    {user?.name ?? "Username"}
                  </b>
                  <span className="timestamp">at {time.toLocaleString()}</span>
                </div>
                <Callout compact={true}>
                  <Markdown>{comment.text}</Markdown>
                </Callout>
              </div>
            </div>
          );
        })}
      </PopupSection>

      <Divider />

      <PopupSection
        title="Danger Zone"
        icon="warning-sign"
        inline={
          <>
            <Button icon="trash" text="Delete card" intent={Intent.DANGER} onClick={() => setDeleteAlertOpen(true)} />
            <Alert
              confirmButtonText="Delete card"
              cancelButtonText="Cancel"
              intent={Intent.DANGER}
              isOpen={deleteAlertOpen}
              loading={deleteAlertLoading}
              onCancel={() => setDeleteAlertOpen(false)}
              onConfirm={() => deleteCard()}
            >
              <p>Are you sure you want to delete this card? This may or may not be able to be undone.</p>
            </Alert>
          </>
        }
      />
    </>
  );
}

export function CardPopup({ cardHint, columnHint }: { cardHint?: ApiCard; columnHint?: ApiColumn }) {
  const navigate = useNavigate();
  const { cardId, boardId } = useParams() as { cardId: ID<"c">; boardId: ID<"b"> };
  const [data, setData] = useState<ApiCardResponse | null>(null);
  const [isOpen, setOpen] = useState(true);

  // TODO: skeleton, maybe? not sure
  let content = <Spinner size={SpinnerSize.LARGE} className="margin-2" />;
  let title = cardHint?.title;
  let columnName = columnHint?.name;

  const leave = () => navigate(`/board/${boardId}`);
  const close = () => setOpen(false);

  const refetch = useCallback(async (): Promise<void> => {
    try {
      setData(await apiFetch<ApiCardResponse>(`/board/${boardId}/card/${cardId}`));
    } catch (error) {
      toastErrorHandler(error);
    }
  }, [boardId, cardId]);

  const boardContext = useContext(BoardContext);
  const rename = useCallback(
    (previousTitle: string) => async (title: string) => {
      if (previousTitle === title.trim()) return;
      try {
        await apiFetch(`/board/${boardId}/card/${cardId}/title`, "POST", { title: title });
        refetch();
        boardContext.refetchBoard();
      } catch (error) {
        toastErrorHandler(error);
      }
    },
    [boardId, cardId, refetch, boardContext]
  );

  useEffect(() => {
    refetch();
  }, [refetch]);

  if (data) {
    title = data.card.title;
    columnName = data.column.name;
    content = (
      <DialogBody>
        <PopupContent />
      </DialogBody>
    );
  }

  const titleElement =
    title && columnName ? (
      <EntityTitle
        className="card-popup-heading"
        title={<EditableText defaultValue={title} onConfirm={rename(title)} />}
        icon="label"
        subtitle={
          <span>
            In column <Code>{columnName}</Code>
          </span>
        }
      />
    ) : (
      <EntityTitle
        className="card-popup-heading"
        loading={true}
        title="Lorem Ipsum"
        icon="label"
        subtitle={<span>In column column name</span>}
      />
    );

  const context: CardContextType = {
    card: data ?? undefined,
    id: cardId,
    tags: data ? data.card.tags.split(",") : [],
    refetchCard: refetch,
    setCard: setData,
    setPopupOpen: setOpen,
  };
  return (
    <CardContext.Provider value={context}>
      <Dialog
        isOpen={isOpen}
        onClosed={leave}
        onClose={close}
        title={titleElement}
        className="card-popup dialog-top"
        portalClassName="card-portal"
      >
        {content}
      </Dialog>
    </CardContext.Provider>
  );
}
