import { DragHandleVertical, IconSize } from "@blueprintjs/icons";
import { MouseEvent, DragEvent, useRef, useState } from "react";
import { classes } from "../util";
import React from "react";

export function DragHandle({ disabled }: { disabled?: boolean }) {
  return (
    <DragHandleVertical
      className={classes({ "drag-handle": true, disabled: disabled ?? false })}
      size={IconSize.LARGE}
    />
  );
}

interface DraggableData<T> {
  disabled: boolean;
  order: readonly T[];
  dragged: T | undefined;
  onMouseDown: (e: MouseEvent<HTMLElement>) => void;
  onDragStart: (e: DragEvent<HTMLElement>) => void;
  onDragEnd: () => Promise<void>;
  onDrop: () => Promise<void>;
  onDragOver: (e: DragEvent<HTMLElement>) => void;
}

export function useDraggable<T>(
  originalOrder: readonly T[],
  onMove: (dragged: T, newIndex: number) => Promise<void>
): DraggableData<T> {
  const [isDragDisabled, setDragDisabled] = useState(false);
  const clickRef = useRef<Node | undefined>();
  const [dragged, setDragged] = useState<T | undefined>();
  const [dragSwap, setDragSwap] = useState<T | "end" | undefined>();
  const newOrder = [...originalOrder];

  if (dragged && dragSwap && dragged !== dragSwap) {
    const baseIndex = originalOrder.indexOf(dragged);
    newOrder.splice(baseIndex, 1);
    if (dragSwap === "end") {
      newOrder.push(dragged);
    } else {
      const newIndex = newOrder.indexOf(dragSwap);
      newOrder.splice(newIndex, 0, dragged);
    }
  }

  const mouseDownHandler = (e: MouseEvent<HTMLElement>) => {
    clickRef.current = e.target as Node;
  };

  const dragStartHandler = (e: DragEvent<HTMLElement>) => {
    if (isDragDisabled) {
      e.preventDefault();
      return;
    }
    const dragHandle = e.currentTarget.querySelector(".drag-handle");
    if (!dragHandle) {
      e.preventDefault();
      return;
    }
    const target = clickRef.current;
    if (!target) {
      e.preventDefault();
      return;
    }
    if (dragHandle.contains(target)) {
      e.dataTransfer.setData("text/plain", "");
      // const img = new Image();
      // img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=";
      // e.dataTransfer.setDragImage(img, 0, 0);
      setDragged(e.currentTarget.dataset.id as T);
    } else {
      e.preventDefault();
    }
  };

  const dropHandler = async () => {
    if (dragged) {
      setDragDisabled(true);
      const newIndex = newOrder.indexOf(dragged);
      const oldIndex = originalOrder.indexOf(dragged);
      if (oldIndex !== newIndex) {
        await onMove(dragged, newIndex);
      }
      setDragDisabled(false);
    }
    setDragged(undefined);
    setDragSwap(undefined);
  };

  const dragEndHandler = async () => {
    if (!isDragDisabled) {
      setDragged(undefined);
      setDragSwap(undefined);
    }
  };

  const isBefore = (a: T, b: T) => newOrder.indexOf(a) > newOrder.indexOf(b);
  const next = (a: T) => (newOrder[newOrder.indexOf(a) + 1] as T | undefined) ?? "end";

  const dragOverHandler = (e: DragEvent<HTMLElement>) => {
    const dragging = dragged!;
    const hovering = e.currentTarget.dataset.id as T;
    const before = isBefore(dragging, hovering);
    setDragSwap(before ? hovering : next(hovering));
    e.preventDefault();
  };

  return {
    disabled: isDragDisabled,
    order: newOrder,
    dragged: dragged,
    onMouseDown: mouseDownHandler,
    onDragStart: dragStartHandler,
    onDragEnd: dragEndHandler,
    onDragOver: dragOverHandler,
    onDrop: dropHandler,
  };
}

class Sentinel<C> {
  column: C;
  constructor(column: C) {
    this.column = column;
  }
}

interface DraggableColumnedData<T, C> {
  disabled: boolean;
  order: readonly (readonly [T, C])[];
  dragged: T | undefined;
  onMouseDown: (e: MouseEvent<HTMLElement>) => void;
  onDragStart: (e: DragEvent<HTMLElement>) => void;
  onDragOver: (e: DragEvent<HTMLElement>) => void;
  onDrop: (e: DragEvent<HTMLElement>) => void;
}

document.addEventListener("dragover", function (event) {
  event.preventDefault();
});

// TODO: there's definitely places to optimize here, maybe make a lookup thingy for all those findIndex calls
function eq<T, C>([ta, ca]: readonly [T, C]) {
  return ([tb, cb]: readonly [T, C]) => ta === tb && ca === cb;
}

export function useDraggableColumned<T, C>(
  originalOrder: readonly (readonly [T, C])[],
  onMove: (dragged: T, newIndex: number, newColumn: C) => Promise<void>
): DraggableColumnedData<T, C> {
  type V = readonly [T, C];
  const [isDragDisabled, setDragDisabled] = useState(false);
  const clickRef = useRef<Node | undefined>();
  const [dragged, setDragged] = useState<V | undefined>();
  const [dragSwap, setDragSwap] = useState<V | Sentinel<C> | undefined>();
  const newOrder = [...originalOrder];

  const heightRef = useRef(0);

  // HACK: https://github.com/facebook/react/issues/1355
  const dragRef = useRef<V | undefined>();
  dragRef.current = dragged;
  const newOrderRef = useRef<V[]>([]);
  newOrderRef.current = newOrder;
  const dragDisabledRef = useRef(false);
  dragDisabledRef.current = isDragDisabled;
  const abortRef = useRef(new AbortController());

  if (dragged) {
    const draggedIndex = newOrder.findIndex(([t]) => t === dragged[0])!;
    newOrder.splice(draggedIndex, 1, dragged);
  }

  if (dragged && dragSwap && (dragSwap instanceof Sentinel || !eq(dragged)(dragSwap))) {
    const baseIndex = newOrder.findIndex(eq(dragged));
    newOrder.splice(baseIndex, 1);
    if (dragSwap instanceof Sentinel) {
      newOrder.push([dragged[0], dragSwap.column]);
    } else {
      const newIndex = newOrder.findIndex(eq(dragSwap));
      newOrder.splice(newIndex, 0, dragged);
    }
  }

  const mouseDownHandler = (e: MouseEvent<HTMLElement>) => {
    clickRef.current = e.target as Node;
  };

  const column = (t: T): C => newOrder.find(([i]) => i === t)![1];

  const dragStartHandler = (e: DragEvent<HTMLElement>) => {
    if (isDragDisabled) {
      e.preventDefault();
      return;
    }
    const dragHandle = e.currentTarget.classList.contains("drag-handle")
      ? e.currentTarget
      : e.currentTarget.querySelector(".drag-handle");
    if (!dragHandle) {
      e.preventDefault();
      return;
    }
    const target = clickRef.current;
    if (!target) {
      e.preventDefault();
      return;
    }
    if (dragHandle.contains(target)) {
      e.dataTransfer.setData("text/plain", "handle");
      // const img = new Image();
      // img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=";
      // e.dataTransfer.setDragImage(img, 0, 0);
      const t = e.currentTarget.dataset.id as T;
      setDragged([t, column(t)]);
      // HACK: https://github.com/facebook/react/issues/1355
      abortRef.current = new AbortController();
      e.currentTarget.addEventListener("dragend", dragEndHandler as unknown as EventListener, {
        signal: abortRef.current.signal,
      });
      heightRef.current = e.currentTarget.clientHeight;
    } else {
      e.preventDefault();
    }
  };

  const dropHandler = React.useCallback(async () => {
    const dragged = dragRef.current;
    const newOrder = newOrderRef.current;

    if (dragged) {
      dragDisabledRef.current = true; // Tentative fix
      setDragDisabled(true);
      const newC = dragged[1];
      const newColumn = newOrder.filter(([, c]) => c === newC);
      const newIndex = newColumn.findIndex(eq(dragged));

      const oldC = originalOrder.find(([t]) => t === dragged[0])![1];
      const oldColumn = originalOrder.filter(([, c]) => c === oldC);
      const oldIndex = oldColumn.findIndex(([t]) => t === dragged[0]);

      if (oldC !== newC || oldIndex !== newIndex) {
        await onMove(dragged[0], newIndex, newC);
      }
      setDragDisabled(false);
    }

    setDragged(undefined);
    setDragSwap(undefined);
  }, [onMove, originalOrder]);

  const dragEndHandler = React.useCallback(async () => {
    abortRef.current.abort();
    if (!dragDisabledRef.current) {
      setDragged(undefined);
      setDragSwap(undefined);
    }
  }, []);

  const isBefore = (a: V, b: V) => newOrder.findIndex(eq(a)) > newOrder.findIndex(eq(b));
  const next = (a: V) => {
    const directNext = newOrder[newOrder.findIndex(eq(a)) + 1] as V | undefined;
    if (!directNext) return new Sentinel(a[1]);
    if (a[1] !== directNext[1]) return new Sentinel(a[1]);
    return directNext;
  };

  const dragOverHandler = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();

    // Avoid flickering
    const heightDifference = Math.max(e.currentTarget.clientHeight - heightRef.current, 0);
    const safePoint = e.currentTarget.offsetTop + heightDifference;
    const safe = e.clientY > safePoint;
    if (!safe) return;

    const dragging = dragged!;
    const end = e.currentTarget.dataset.end as C | undefined;
    let newDragSwap: V | Sentinel<C>;
    if (end) {
      newDragSwap = new Sentinel(end);
    } else {
      const t = e.currentTarget.dataset.id as T;
      const hovering = [t, column(t)] as const;
      const before = isBefore(dragging, hovering);
      newDragSwap = before ? hovering : next(hovering);
    }

    if (
      (Array.isArray(dragSwap) &&
        Array.isArray(newDragSwap) &&
        newDragSwap[0] === dragSwap[0] &&
        newDragSwap[1] === dragSwap[1]) ||
      (dragSwap instanceof Sentinel && newDragSwap instanceof Sentinel && dragSwap.column === newDragSwap.column)
    ) {
      return; // don't create new arrays for no reason
    }

    if (Array.isArray(newDragSwap) && newDragSwap[1] !== dragging[1]) {
      setDragged([dragging[0], newDragSwap[1]]);
    } else if (newDragSwap instanceof Sentinel && newDragSwap.column !== dragging[1]) {
      setDragged([dragging[0], newDragSwap.column]);
    }

    setDragSwap(newDragSwap);
  };

  return {
    disabled: isDragDisabled,
    order: newOrder,
    dragged: dragged?.[0],
    onMouseDown: mouseDownHandler,
    onDragStart: dragStartHandler,
    onDragOver: dragOverHandler,
    onDrop: dropHandler,
  };
}
