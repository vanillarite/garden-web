import { Intent, NonIdealState, Spinner, SpinnerSize } from "@blueprintjs/core";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { supabase, ApiUser, apiFetch } from "../api";
import { AppToaster, toastErrorHandler, App } from "../App";

export default function ReauthPage() {
  const navigate = useNavigate();
  useEffect(() => {
    (async () => {
      try {
        const {
          data: { session },
        } = await supabase.auth.getSession();
        if (!session) {
          navigate("/login");
          return;
        }
        const login = await apiFetch<string>(`/auth/supabase_login`, "POST", { jwt: session.access_token });
        if (login === "register") {
          const invite = window.localStorage.getItem("invite");
          if (invite) {
            window.localStorage.removeItem("invite");
            navigate(`/register/${invite}`);
          } else {
            navigate("/register");
          }
        } else {
          localStorage.setItem("token", login);
          const user = await apiFetch<ApiUser>("/me");
          AppToaster.then((toaster) => toaster.show({ intent: Intent.SUCCESS, message: `Logged in as ${user.name}` }));
          window.localStorage.removeItem("invite");
          window.localStorage.setItem("user", JSON.stringify(user));
          navigate("/home");
        }
      } catch (error) {
        toastErrorHandler(error);
      }
    })();
  }, [navigate]);

  return App(<NonIdealState icon={<Spinner size={SpinnerSize.LARGE} />} />);
}
