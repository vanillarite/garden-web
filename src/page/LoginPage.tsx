import { useEffect, useState } from "react";
import { App, toastErrorHandler } from "../App";
import { apiFetch, ApiInviteResponse, ID } from "../api";
import { useNavigate, useParams } from "react-router-dom";
import { Session } from "@supabase/supabase-js";
import { Auth } from "@supabase/auth-ui-react";
import { ThemeSupa } from "@supabase/auth-ui-shared";
import { supabase } from "../api";
import { Callout, Classes, H3, Intent } from "@blueprintjs/core";

export default function LoginPage() {
  const { inviteId } = useParams() as { inviteId?: ID<"i"> };
  const [session, setSession] = useState<Session | null>(null);
  const [inviteData, setInviteData] = useState<ApiInviteResponse | null>(null);
  const navigate = useNavigate();

  useEffect(() => {
    supabase.auth.getSession().then(({ data: { session } }) => {
      setSession(session);
    });
  }, []);

  useEffect(() => {
    if (inviteId) {
      window.localStorage.setItem("invite", inviteId);
      apiFetch<ApiInviteResponse>(`/invite/${inviteId}`)
        .then((ok) => {
          setInviteData(ok);
        })
        .catch((error) => {
          toastErrorHandler(error);
          navigate("/register");
        });
    }
  }, [inviteId, navigate]);

  if (session) {
    navigate("/reauth");
  }

  const hostname = document.location.origin;

  return App(
    <div>
      <Callout intent={Intent.PRIMARY}>
        Since I don&apos;t want to be response for managing passwords, please use one of these services instead so that
        they can handle all security for me instead.
      </Callout>
      <div className="fit flex-center margin-auto">
        {inviteId && !inviteData && <H3 className={Classes.SKELETON}>foobar has invited you to barbaz</H3>}
        {inviteId && inviteData && (
          <H3 className="thin-header text-center">
            <b>{inviteData.user.name}</b> has invited you to <b>{inviteData.org.name}</b>
          </H3>
        )}
      </div>
      {!session && (
        <div className="fit flex-center margin-auto">
          <Auth
            supabaseClient={supabase}
            appearance={{ theme: ThemeSupa }}
            onlyThirdPartyProviders={true}
            providers={["google", "discord"]}
            redirectTo={`${hostname}/#/callback`}
            theme="dark"
          />
        </div>
      )}
    </div>
  );
}
