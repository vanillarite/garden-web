import { Card, Divider, Elevation, H2, H3, NonIdealState, Spinner, SpinnerSize } from "@blueprintjs/core";
import { App, toastErrorHandler } from "../App";
import { useEffect, useState } from "react";
import { apiFetch, ApiMeBoardsResponse } from "../api";
import React from "react";
import { useNavigate } from "react-router-dom";

export default function HomePage() {
  const navigate = useNavigate();
  const [boards, setBoards] = useState<ApiMeBoardsResponse | null>(null);
  let content = <NonIdealState icon={<Spinner size={SpinnerSize.LARGE} />} />;

  useEffect(() => {
    (async () => {
      try {
        setBoards(await apiFetch<ApiMeBoardsResponse>("/me/boards"));
      } catch (error) {
        toastErrorHandler(error);
      }
    })();
  }, []);

  if (boards) {
    content = (
      <div className="inter">
        <H2>Your boards</H2>
        <Divider />
        {boards.orgs.map((org) => (
          <React.Fragment key={org.id}>
            <H3>{org.name}</H3>
            <div className="g-row">
              {boards.boards
                .filter((b) => b.organization_id === org.id)
                .map((board) => (
                  <Card
                    key={board.id}
                    interactive={true}
                    elevation={Elevation.TWO}
                    className="g-menu-card"
                    onClick={() => navigate(`/board/${board.id}`)}
                  >
                    <span>{board.name}</span>
                  </Card>
                ))}
            </div>
          </React.Fragment>
        ))}
      </div>
    );
  }

  return App(content, "Home");
}
