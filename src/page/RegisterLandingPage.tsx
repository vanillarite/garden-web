import { Button, FormGroup, InputGroup, Intent, NonIdealState } from "@blueprintjs/core";
import { App } from "../App";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Control } from "@blueprintjs/icons/lib/esm/generated/20px/paths";

export default function RegisterLandingPage() {
  const [invite, setInvite] = useState("");
  const navigate = useNavigate();
  const submit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (invite !== "") {
      navigate(`/register/${invite}`);
    }
  };

  return App(
    <NonIdealState
      icon={
        <span aria-hidden="true" tabIndex={-1} className="bp5-icon bp5-icon-control bp5-icon-muted">
          <svg data-icon="control" height="48" role="img" viewBox="0 0 20 20" width="48">
            <path d={Control[0]} fillRule="evenodd"></path>
          </svg>
        </span>
      }
      title={
        <span>
          Welcome to <span className="sc">Garden</span>
        </span>
      }
      description="You need an invitation to use this website. If you have one, insert it below"
    >
      <form className="fit flex-center margin-auto" onSubmit={submit}>
        <FormGroup label="Invite code" labelFor="form-invite" fill={false}>
          <InputGroup id="form-invite" name="invite" onValueChange={(str) => setInvite(str)} />
        </FormGroup>
        <Button text="Submit" intent={Intent.SUCCESS} type="submit" />
      </form>
    </NonIdealState>
  );
}
