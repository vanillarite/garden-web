import { createRoot } from "react-dom/client";
import "./style/index.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import { API, apiFetch, type JwtData } from "./api";
import { createHashRouter, RouterProvider } from "react-router-dom";
import ErrorPage from "./page/ErrorPage";
import { createContext, StrictMode, useState } from "react";
import LoginPage from "./page/LoginPage";
import HomePage from "./page/HomePage";
import LandingPage from "./page/LandingPage";
import BoardPage from "./page/BoardPage";
import { AppToaster, toastErrorHandler } from "./App";
import { Intent, OverlaysProvider } from "@blueprintjs/core";
import CallbackPage from "./page/CallbackPage";
import ReauthPage from "./page/ReAuthPage";
import RegisterPage from "./page/RegisterPage";
import RegisterLandingPage from "./page/RegisterLandingPage";

const router = createHashRouter([
  {
    path: "/",
    element: <LandingPage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
  {
    path: "/login/:inviteId",
    element: <LoginPage />,
  },
  {
    path: "/register/",
    element: <RegisterLandingPage />,
  },
  {
    path: "/register/:inviteId",
    element: <RegisterPage />,
  },
  {
    path: "/callback",
    element: <CallbackPage />,
  },
  {
    path: "/reauth",
    element: <ReauthPage />,
  },
  {
    path: "/home",
    element: <HomePage />,
  },
  {
    path: "/board/:boardId",
    element: <BoardPage />,
  },
  {
    path: "/board/:boardId/card/:cardId",
    element: <BoardPage />,
  },
]);

export const ApiVersion = createContext<string | null>(null);

// TODO: go to reauth if needed
(async function checkToken() {
  const debugConvertTime = (total: number): string => {
    const h = Math.floor(total / (60 * 60));
    const m = Math.floor((total - h * 60 * 60) / 60);
    const s = total - h * 60 * 60 - m * 60;
    return `${h}h ${m}m ${s}s`;
  };
  const token = localStorage.getItem("token");
  if (token) {
    const data = JSON.parse(atob(token.split(".")[1])) as JwtData;
    const exp = data.exp;
    const iss = data.iss;
    const now = Math.floor(Date.now().valueOf() / 1000);
    const remaining = exp - now;
    const sinceIssue = now - iss;
    const refreshThreshold = (exp - iss) * (2 / 3);
    const refreshIn = remaining - refreshThreshold;

    console.log(
      `Auth check: Issued ${debugConvertTime(sinceIssue)} ago; Expiry in ${debugConvertTime(remaining)}; Refresh in ${
        refreshIn <= 0 ? "now" : debugConvertTime(refreshIn)
      }`
    );
    if (refreshIn <= 0) {
      console.log("REFRESH!");
      try {
        const ok = await apiFetch<string>("/auth/refresh", "POST");
        localStorage.setItem("token", ok);
      } catch (err) {
        // just let it expire?
        toastErrorHandler(err);
        window.setTimeout(() => {
          AppToaster.then((toaster) => {
            toaster.show({ intent: Intent.WARNING, message: "Could not refresh auth token" });
          });
        }, 10); // wtf?
        // TODO: make a queue somewhere or something for these toasts to not glitch as much
      }
    }
  }
  window.setTimeout(checkToken, 1000 * 60 * 30); // 30 minutes
})();

function Wrapper() {
  const [version, setVersion] = useState<string | null>(null);
  if (version === null) {
    fetch(`${API}/api/version`)
      .then((resp) => resp.text())
      .then((text) => setVersion(text))
      .catch((err) => console.error(err));
  }
  return (
    <StrictMode>
      <ApiVersion.Provider value={version}>
        <OverlaysProvider>
          <RouterProvider router={router} />
        </OverlaysProvider>
      </ApiVersion.Provider>
    </StrictMode>
  );
}

createRoot(document.getElementById("root")!).render(<Wrapper />);
