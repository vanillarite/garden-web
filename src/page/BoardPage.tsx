import { Checkbox, Divider, NonIdealState, Spinner, SpinnerSize, Tag } from "@blueprintjs/core";
import React, { createContext, useCallback, useEffect, useState } from "react";
import { App, toastErrorHandler } from "../App";
import { useParams } from "react-router-dom";
import { ApiBoardResponse, apiFetch, ApiTag, ID } from "../api";
import { KanbanColumn, KanbanCard, AddColumn, AddCard } from "../components/boardComponents";
import { CardPopup } from "../components/cardComponents";
import { convertTagsToCardTagElements } from "../components/tagComponents";
import { useDraggableColumned } from "../components/dragComponents";
import { classes } from "../util";
import { AlignLeft, Comment } from "@blueprintjs/icons";

interface BoardContextType {
  board?: ApiBoardResponse;
  id: ID<"b">;
  tags: readonly ApiTag[];
  refetchBoard: () => Promise<void>;
  setBoard: (board: ApiBoardResponse) => void;
}
export const BoardContext = createContext<BoardContextType>(null!); // Meta thinks they know better than developers

function BoardContent({
  data,
  boardId,
  refetch,
  debug,
}: {
  data: ApiBoardResponse;
  boardId: ID<"b">;
  refetch: () => Promise<void>;
  debug: boolean;
}) {
  const columnOrder = [...data.columns].sort((a, b) => a.idx - b.idx);
  const columnIdOrder = columnOrder.map((i) => i.id);
  const cardOrder = [...data.cards]
    .sort((a, b) => a.idx - b.idx)
    .sort((a, b) => columnIdOrder.indexOf(a.column_id) - columnIdOrder.indexOf(b.column_id))
    .map((i) => [i.id, i.column_id] as const);
  const move = async (dragged: ID<"c">, newIndex: number, newColumn: ID<"col">) => {
    try {
      await apiFetch<void>(`/board/${boardId}/card/${dragged}/move`, "POST", {
        index: newIndex.toString(),
        column: newColumn,
      });
      await refetch();
    } catch (error) {
      toastErrorHandler(error);
    }
  };
  const drag = useDraggableColumned(cardOrder, move);

  return (
    <div className="inter g-board">
      {columnOrder.map((column) => (
        <React.Fragment key={column.id}>
          <KanbanColumn title={debug ? `${column.idx}. ${column.id}` : column.name}>
            {drag.order
              .filter(([, columnId]) => columnId === column.id)
              .map(([cardId]) => data.cards.find((j) => j.id === cardId)!)
              .filter((card) => !card.deleted)
              .map((card) => (
                <KanbanCard
                  key={card.id}
                  data-id={card.id}
                  draggable="true"
                  className={classes({ dragging: card.id === drag.dragged, "drag-handle": true })}
                  onDragStart={drag.onDragStart}
                  onDragOver={drag.onDragOver}
                  onDrop={drag.onDrop}
                  onDragEnter={(e) => e.preventDefault()}
                  onMouseDown={drag.onMouseDown}
                  link={`/board/${boardId}/card/${card.id}`}
                >
                  <div className="tag-container">{convertTagsToCardTagElements(card.tags, data?.tags ?? [])}</div>
                  <span>{debug ? `${card.idx}. ${card.id}` : card.title}</span>
                  {(card.has_description || card.comment_count !== 0) && (
                    <div className="card-properties">
                      {card.has_description && <Tag minimal={true} icon={<AlignLeft />} title="Has description" />}
                      {card.comment_count !== 0 && (
                        <Tag
                          minimal={true}
                          icon={<Comment />}
                          title={`Has ${card.comment_count} comment${card.comment_count === 1 ? "" : "s"}`}
                        >
                          {card.comment_count}
                        </Tag>
                      )}
                    </div>
                  )}
                </KanbanCard>
              ))}
            <div className="g-card-width" data-end={column.id} onDragOver={drag.onDragOver}>
              <AddCard retrigger={refetch} board={boardId} column={column.id} />
            </div>
          </KanbanColumn>
          <Divider />
        </React.Fragment>
      ))}
      <KanbanColumn>
        <AddColumn retrigger={refetch} board={boardId} />
      </KanbanColumn>
    </div>
  );
}

export default function BoardPage() {
  const { boardId, cardId } = useParams() as { boardId: ID<"b">; cardId?: ID<"c"> };

  const [data, setData] = useState<ApiBoardResponse | null>(null);
  const [debug, setDebug] = useState(false);

  // TODO: skeleton
  let content = <NonIdealState icon={<Spinner size={SpinnerSize.LARGE} />} />;
  let overlay = null;
  let name: string | undefined;

  const refetch = useCallback(async (): Promise<void> => {
    try {
      setData(await apiFetch<ApiBoardResponse>(`/board/${boardId}`));
    } catch (error) {
      toastErrorHandler(error);
    }
  }, [boardId]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  if (data) {
    name = `${data.org.name} \u203a ${data.board.name}`;
    content = <BoardContent data={data} boardId={boardId} refetch={refetch} debug={debug} />;
  }

  if (cardId) {
    const cardHint = data?.cards.find((c) => c.id === cardId);
    const columnHint = cardHint ? data?.columns.find((c) => c.id === cardHint.column_id) : undefined;
    overlay = <CardPopup key={cardId} cardHint={cardHint} columnHint={columnHint} />;
  }

  const context: BoardContextType = {
    board: data ?? undefined,
    id: boardId,
    tags: data ? data.tags : [],
    refetchBoard: refetch,
    setBoard: setData,
  };
  return App(
    <BoardContext.Provider value={context}>
      {overlay}
      {content}
    </BoardContext.Provider>,
    name,
    <Checkbox checked={debug} onChange={(e) => setDebug(e.currentTarget.checked)} label="idx debug" />
  );
}
