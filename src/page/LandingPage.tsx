import { Button, ButtonGroup, NonIdealState } from "@blueprintjs/core";
import { App } from "../App";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Control } from "@blueprintjs/icons/lib/esm/generated/20px/paths";

export default function LandingPage() {
  const navigate = useNavigate();
  const loggedIn = window.localStorage.getItem("token") !== null;

  useEffect(() => {
    if (loggedIn) navigate("/home");
  }, [loggedIn, navigate]);

  return App(
    loggedIn ? (
      <></>
    ) : (
      <NonIdealState
        icon={
          <span aria-hidden="true" tabIndex={-1} className="bp5-icon bp5-icon-control bp5-icon-muted">
            <svg data-icon="control" height="48" role="img" viewBox="0 0 20 20" width="48">
              <path d={Control[0]} fillRule="evenodd"></path>
            </svg>
          </span>
        }
        title={
          <span>
            Welcome to <span className="sc">Garden</span>
          </span>
        }
        description="You need an invitation to use this website"
      >
        <ButtonGroup fill={true}>
          <Button text="Log in" onClick={() => navigate("/login")} />
          <Button text="Use invite" onClick={() => navigate("/register")} />
        </ButtonGroup>
      </NonIdealState>
    )
  );
}
