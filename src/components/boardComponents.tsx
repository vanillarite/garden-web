import {
  Button,
  ButtonGroup,
  Card,
  CardProps,
  Classes,
  ControlGroup,
  EditableText,
  Elevation,
  EntityTitle,
  H4,
  InputGroup,
  Intent,
} from "@blueprintjs/core";
import { useState, FormEvent } from "react";
import { ID, ApiColumn, apiFetch, ApiUser, API, API_PREFIX } from "../api";
import { AppToaster } from "../App";
import { useNavigate } from "react-router-dom";

export function KanbanColumn({ title, children }: { title?: string; children?: React.ReactNode }) {
  return (
    <div className="g-column">
      {title && <EntityTitle heading={H4} title={title} />}
      {children}
    </div>
  );
}

interface KanbanCardProps {
  children?: React.ReactNode;
  link?: string;
}
export function KanbanCard({ children, link, className, ...rest }: KanbanCardProps & CardProps) {
  const navigate = useNavigate();
  const onClick = link === undefined ? undefined : () => navigate(link);
  const classes = className ? `${className} g-card more-compact` : "g-card more-compact";
  return (
    <Card interactive={true} compact={true} elevation={Elevation.TWO} className={classes} onClick={onClick} {...rest}>
      {children}
    </Card>
  );
}

function AddCommon({
  request,
  retrigger,
  buttonTitle,
  input,
}: {
  request: (form: HTMLFormElement) => Promise<unknown>;
  retrigger: () => Promise<void>;
  buttonTitle: string;
  input: (disabled: boolean) => JSX.Element;
}) {
  const [focused, setFocused] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    if (!loading) {
      setLoading(true);

      request(e.currentTarget)
        .catch((err: Error) => {
          AppToaster.then((toaster) => toaster.show({ intent: Intent.DANGER, message: err.message }));
        })
        .finally(() => {
          retrigger().then(() => {
            setLoading(false);
            setFocused(false);
            // form.reset();
          });
        });
    }
  };

  if (focused) {
    return (
      <div className="g-card-width">
        <form onSubmit={handleSubmit}>
          <ControlGroup fill={true} vertical={true}>
            {/* <InputGroup placeholder="Name of this column" name="name" disabled={loading} /> */}
            {input(loading)}
            <ButtonGroup>
              <Button
                intent={Intent.PRIMARY}
                icon="plus"
                fill={true}
                text="Create"
                loading={loading}
                type="submit"
                name="name"
              />
              <Button
                intent={Intent.DANGER}
                icon="cross"
                disabled={loading}
                onClick={() => {
                  if (!loading) setFocused(false);
                }}
              />
            </ButtonGroup>
          </ControlGroup>
        </form>
      </div>
    );
  } else {
    return (
      <Button
        className="g-card-width"
        icon="insert"
        text={buttonTitle}
        alignText="left"
        onClick={() => setFocused(true)}
      />
    );
  }
}

export function AddColumn({ retrigger, board }: { retrigger: () => Promise<void>; board: ID<"b"> }) {
  return (
    <AddCommon
      retrigger={retrigger}
      buttonTitle="Add another column"
      input={(disabled) => <InputGroup placeholder="Name of this column" name="name" disabled={disabled} />}
      request={(form: HTMLFormElement) => {
        const body = new FormData(form);
        return apiFetch<ApiColumn>(`/board/${board}/column`, "POST", body);
      }}
    />
  );
}

export function AddCard({
  retrigger,
  board,
  column,
}: {
  retrigger: () => Promise<void>;
  board: ID<"b">;
  column: ID<"col">;
}) {
  return (
    <AddCommon
      retrigger={retrigger}
      buttonTitle="Add new card"
      input={(disabled) => (
        <KanbanCard>
          <EditableText
            maxLength={100}
            multiline={true}
            className="card-title"
            alwaysRenderInput={true}
            disabled={disabled}
          />
        </KanbanCard>
      )}
      request={(form: HTMLFormElement) => {
        const body = new FormData(form);
        const textarea = form.querySelector(".card-title textarea") as HTMLTextAreaElement;
        body.append("title", textarea.value);
        return apiFetch<ApiColumn>(`/board/${board}/column/${column}/card`, "POST", body);
      }}
    />
  );
}

export function Avatar({ user }: { user?: ApiUser }) {
  if (!user) return <img className={`avatar ${Classes.SKELETON}`} width={36} height={36} />;

  const params = new URLSearchParams({ url: user.avatar });
  const url = `${API}${API_PREFIX}/relay/avatar/${user.id}?${params.toString()}`;
  return <img className="avatar" src={url} width={36} height={36} />;
}
