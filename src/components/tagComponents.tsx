import {
  Button,
  Callout,
  Checkbox,
  Classes,
  FormGroup,
  InputGroup,
  Intent,
  Panel,
  PanelProps,
  PanelStack2,
  RadioCard,
  RadioGroup,
  Tag,
} from "@blueprintjs/core";
import { TagColor, ID, TAG_COLORS, apiFetch, ApiTag, ApiTagsResponse } from "../api";
import { useState, useCallback, FormEvent, MouseEvent, useContext, useEffect, useRef } from "react";
import { toastErrorHandler } from "../App";
import { classes, titleCase } from "../util";
import { BoardContext } from "../page/BoardPage";
import { CardContext } from "./cardComponents";
import { DragHandle, useDraggable } from "./dragComponents";

interface CardTagProps {
  color: TagColor;
  text: string;
  large?: boolean;
  loading?: boolean;
  onClick?: (e: React.MouseEvent<HTMLSpanElement>) => void;
}
export function CardTag({ color, text, large, loading, onClick }: CardTagProps) {
  const labelClasses = loading ? Classes.SKELETON : `color-override tag-color-${color}`;
  return (
    <Tag className={labelClasses} large={large} interactive={true} onClick={onClick}>
      {text}
    </Tag>
  );
}

type IncompleteTag = readonly [ApiTag | undefined, string];
function sortIncompleteTags([aTag, aId]: IncompleteTag, [bTag, bId]: IncompleteTag): number {
  if (aTag !== undefined && bTag !== undefined) {
    return aTag.idx - bTag.idx;
  } else if (aTag === undefined && bTag !== undefined) {
    return 1;
  } else if (aTag !== undefined && bTag === undefined) {
    return -1;
  } else {
    return +(aId > bId) - +(aId < bId);
  }
}

export function convertTagsToCardTagElements(
  cardTags: string,
  globalTags: readonly ApiTag[],
  props?: Partial<CardTagProps>
): JSX.Element[] {
  return cardTags
    .split(",")
    .filter((i) => i !== "")
    .map((tag) => [globalTags.find((i) => i.id === tag), tag] as const)
    .sort(sortIncompleteTags)
    .map(([tag, id]) =>
      tag ? (
        <CardTag key={id} color={tag.color} text={tag.text} {...props} />
      ) : (
        <CardTag key={id} color="blue" text="Loading" loading={true} {...props} onClick={undefined} />
      )
    );
}

function TagSelectorCard(value: TagColor) {
  const label = titleCase(value);
  const colorClass = `tag-color-${value}`;
  return (
    <RadioCard
      className={`color-override ${colorClass}`}
      // label={label}
      key={value}
      value={value}
      title={label}
      compact={true}
      showAsSelectedWhenChecked={false}
    />
  );
}

interface TagCreateEditPanelProps {
  initialName?: string;
  initialColor?: TagColor;
  existingTag?: ID<"t">;
}
function TagCreateEditPanel({
  closePanel,
  initialName,
  initialColor,
  existingTag,
}: PanelProps<TagCreateEditPanelProps>) {
  const [name, setName] = useState(initialName ?? "");
  const changeName = useCallback((value: string) => setName(value), []);
  const [selectedColor, setSelectedColor] = useState<TagColor>(initialColor ?? "default");
  const changeColor = useCallback((event: FormEvent<HTMLInputElement>) => {
    setSelectedColor(event.currentTarget.value as TagColor);
  }, []);
  const [loading, setLoading] = useState(false);
  const boardContext = useContext(BoardContext);
  const boardId = boardContext.board!.board.id;
  const submit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setLoading(true);
    try {
      const endpoint = existingTag ? `/board/${boardId}/tag/${existingTag}` : `/board/${boardId}/tag`;
      await apiFetch<void>(endpoint, "POST", { name: name, color: selectedColor });
      await boardContext?.refetchBoard();
      setLoading(false);
      closePanel();
    } catch (error) {
      setLoading(false);
      toastErrorHandler(error);
    }
  };

  return (
    <form className="tag-selector-inner" onSubmit={submit}>
      <div className="tag-selector-fill">
        <Callout className="tag-selector-preview">
          <CardTag color={selectedColor} text={name} large={true} />
        </Callout>
        <FormGroup label="Text" labelFor="new-tag-text" fill={true} disabled={loading}>
          <InputGroup
            id="new-tag-text"
            name="text"
            onValueChange={changeName}
            defaultValue={initialName}
            disabled={loading}
          />
        </FormGroup>
        <FormGroup label="Color" fill={true} disabled={loading}>
          <RadioGroup
            selectedValue={selectedColor}
            disabled={loading}
            onChange={changeColor}
            className="tag-selector-row"
            name="color"
          >
            {TAG_COLORS.map((i) => TagSelectorCard(i))}
          </RadioGroup>
        </FormGroup>
      </div>
      <Button
        intent={Intent.SUCCESS}
        fill={true}
        type={"submit"}
        text={existingTag ? "Save changes" : "Create"}
        loading={loading}
        disabled={loading}
      />
    </form>
  );
}

interface TagListPanelProps {}
function TagListPanel({ openPanel }: PanelProps<TagListPanelProps>) {
  const boardContext = useContext(BoardContext);
  const cardContext = useContext(CardContext);
  const cardTags = cardContext.tags;
  const globalTags = boardContext.tags;

  const tagOrder: ID<"t">[] = [...globalTags].sort((a, b) => a.idx - b.idx).map((i) => i.id);
  const move = async (dragged: ID<"t">, newIndex: number) => {
    try {
      await apiFetch<ApiTagsResponse>(`/board/${boardContext.id}/tag/${dragged}/move`, "POST", {
        index: newIndex.toString(),
      });
      await boardContext.refetchBoard();
    } catch (error) {
      toastErrorHandler(error);
    }
  };
  const drag = useDraggable(tagOrder, move);

  const [changes, setChanges] = useState<Map<ID<"t">, boolean>>(new Map());
  console.log(changes);

  const clickHandler = (tag: ID<"t">) => (e: MouseEvent<HTMLInputElement>) => {
    const checked = e.currentTarget.checked;
    console.log(tag, checked);
    setChanges(new Map(changes.set(tag, checked)));
    // TODO: when i have a better data store, maybe change the known tags in the store, so it still live updates, but
    // only does one request
  };

  const componentWillUnmount = useRef(false);

  useEffect(() => {
    componentWillUnmount.current = false;
    return () => {
      componentWillUnmount.current = true;
    };
  }, []);

  useEffect(() => {
    return () => {
      if (changes.size === 0) return;
      if (!componentWillUnmount.current) return;

      const add: ID<"t">[] = [];
      const remove: ID<"t">[] = [];
      changes.forEach((value, key) => {
        (value ? add : remove).push(key);
      });
      apiFetch<void>(`/board/${boardContext.id}/card/${cardContext.id}/tag`, "PATCH", {
        add: add.join(","),
        remove: remove.join(","),
      })
        .then(() => {
          setChanges(new Map());
          cardContext.refetchCard();
          boardContext.refetchBoard();
        })
        .catch((err) => {
          toastErrorHandler(err);
        });
    };
  }, [boardContext, cardContext, changes]);

  const openCreatePanel = () =>
    openPanel({
      renderPanel: TagCreateEditPanel,
      title: "Create tag",
    });

  const openEditPanel = (name: string, color: TagColor, tag: ID<"t">) => () =>
    openPanel({
      renderPanel: TagCreateEditPanel,
      props: {
        initialName: name,
        initialColor: color,
        existingTag: tag,
      },
      title: "Edit tag",
    });

  return (
    <div className="tag-selector-inner">
      <div className="tag-selector-fill">
        {drag.order
          .map((i) => globalTags.find((j) => j.id === i)!)
          .map((i) => (
            <span
              key={i.id}
              data-id={i.id}
              draggable="true"
              className={classes({ dragging: i.id === drag.dragged })}
              onDragStart={drag.onDragStart}
              onDragEnd={drag.onDragEnd}
              onDragOver={drag.onDragOver}
              onDrop={drag.onDrop}
              onMouseDown={drag.onMouseDown}
            >
              <Checkbox
                large={true}
                checked={changes.get(i.id) ?? cardTags.includes(i.id)}
                onClick={clickHandler(i.id)}
              >
                <div className="inline-flex fill-width">
                  <CardTag color={i.color} text={i.text} large={true} />
                  <DragHandle disabled={drag.disabled} />
                  <Button icon="edit" className="not-large" onClick={openEditPanel(i.text, i.color, i.id)} />
                </div>
              </Checkbox>
            </span>
          ))}
      </div>
      <Button fill={true} intent={Intent.PRIMARY} text="Create a new tag" onClick={openCreatePanel} />
    </div>
  );
}

export function TagSelectorContainer() {
  const initial: Panel<TagListPanelProps> = {
    renderPanel: TagListPanel,
    title: "Tags",
  };
  return <PanelStack2 showPanelHeader={true} initialPanel={initial} className="tag-selector-container" />;
}
