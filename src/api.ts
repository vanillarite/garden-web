import { createClient } from "@supabase/supabase-js";

export const TAG_COLORS = [
  "green",
  "forest",
  "lime",
  "yellow",
  "gold",
  "vermillon",
  "red",
  "rose",
  "violet",
  "indigo",
  "cerulean",
  "blue",
  "turquoise",
  "sepia",
  "dark-gray",
  "default",
] as const;
export type TagColor = (typeof TAG_COLORS)[number];

export type FetchResponse<T = unknown> = ApiOK<T> | ApiError | FetchError;
type ApiResponse<T = unknown> = ApiOK<T> | ApiError;

export interface ApiOK<T = unknown> {
  status: "ok";
  response: T;
}

export interface ApiError {
  status: "error";
  error: string;
}

export interface FetchError {
  status: "fetcherror";
  error: Error;
}

export interface JwtData {
  exp: number;
  nbf: number;
  iss: number;
  sub: ID<"u">;
  name: string;
}

export type ID<T extends string> = `${T}.${string}`;

export interface Identifiable<T extends string> {
  id: ID<T>;
  created_at: string;
  updated_at: string;
}

export interface ApiUser extends Identifiable<"u"> {
  name: string;
  email?: string;
  avatar: string;
  supabase_id: string;
  level: number;
}

export interface ApiBoard extends Identifiable<"b"> {
  name: string;
  organization_id: string;
}

export interface ApiOrganization extends Identifiable<"org"> {
  name: string;
}

export interface ApiColumn extends Identifiable<"col"> {
  board_id: ID<"b">;
  name: string;
  idx: number;
}

export interface ApiCard extends Identifiable<"c"> {
  board_id: ID<"b">;
  column_id: ID<"col">;
  title: string;
  idx: number;
  tags: string;
  has_description: boolean;
  comment_count: number;
  deleted: boolean;
}

export interface ApiDescription extends Identifiable<"d"> {
  card_id: ID<"c">;
  description: string;
}

export interface ApiTag extends Identifiable<"t"> {
  board_id: ID<"b">;
  text: string;
  color: TagColor;
  idx: number;
}

export interface ApiComment extends Identifiable<"com"> {
  board_id: ID<"b">;
  user_id: ID<"u">;
  text: string;
}

export interface ApiMeBoardsResponse {
  orgs: ApiOrganization[];
  boards: ApiBoard[];
}

export interface ApiBoardResponse {
  org: ApiOrganization;
  board: ApiBoard;
  columns: ApiColumn[];
  cards: ApiCard[];
  tags: ApiTag[];
  members: ApiUser[];
}

export interface ApiCardResponse {
  card: ApiCard;
  column: ApiColumn;
  desc: ApiDescription | null;
  comments: ApiComment[];
}

export interface ApiTagsResponse {
  tags: ApiTag[];
}

export interface ApiInviteResponse {
  org: ApiOrganization;
  user: ApiUser;
}

export class CustomApiError extends Error {
  status: number;
  constructor(message: string, status: number) {
    super(message);
    this.status = status;
  }
}

export const API =
  document.location.hostname === "localhost" ? "http://localhost:3000" : "https://garden-api.rymiel.space";
export const API_PREFIX = "/api";

declare const WEB_VERSION: string;

type AllowedHTTPMethod = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";

export async function apiFetch<T>(
  endpoint: string,
  method?: AllowedHTTPMethod,
  body?: FormData | Record<string, string>,
  recursing?: boolean
): Promise<T> {
  const headers = new Headers();
  const key = localStorage.getItem("token");
  let formBody;
  if (key !== null) {
    headers.set("Authorization", key);
    headers.set("X-Garden-Client", `garden web/${WEB_VERSION} rymiel`);
  }
  if (body instanceof FormData) {
    formBody = body;
  } else if (body !== undefined) {
    formBody = new FormData();
    for (const key in body) {
      if (Object.prototype.hasOwnProperty.call(body, key)) {
        formBody.set(key, body[key]);
      }
    }
  }
  method ??= "GET";
  const response = await fetch(API + API_PREFIX + endpoint, { method, body: formBody, headers });
  const text = await response.text();
  try {
    const json = JSON.parse(text) as ApiResponse<T>;
    if (json.status === "ok") {
      return json.response;
    } else {
      if (!recursing && response.status === 401) {
        localStorage.removeItem("token");
        const {
          data: { session },
        } = await supabase.auth.getSession();
        if (!session) {
          document.location.hash = "#/login";
          throw new CustomApiError(json.error, response.status);
        }
        const login = await apiFetch<string>(`/auth/supabase_login`, "POST", { jwt: session.access_token }, true);
        if (login === "register") {
          document.location.hash = "#/register";
          throw new CustomApiError(json.error, response.status);
        } else {
          localStorage.setItem("token", login);
          return apiFetch(endpoint, method, body, true);
        }
      }
      throw new CustomApiError(json.error, response.status);
    }
  } catch (error) {
    if (error instanceof SyntaxError) {
      throw new CustomApiError(text, response.status);
    } else {
      throw error;
    }
  }
}

export const supabase = createClient(
  "https://liuyviownijnkfufvivi.supabase.co",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzMTg3ODMzNSwiZXhwIjoxOTQ3NDU0MzM1fQ.CzumOjOkaxSYWMvbu0oxPMtBu-oAJyNK7cN7L7REYVo"
);

export function useUser(): ApiUser | undefined {
  const json = window.localStorage.getItem("user");
  if (!json) return undefined;
  return JSON.parse(json) as ApiUser;
}
