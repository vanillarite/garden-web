import {
  Classes,
  NonIdealState,
  Spinner,
  SpinnerSize,
  Callout,
  Intent,
  H4,
  RadioGroup,
  RadioCard,
  InputGroup,
  Button,
  H3,
} from "@blueprintjs/core";
import { Session } from "@supabase/supabase-js";
import React, { useState, useEffect } from "react";
import { ApiInviteResponse, ApiUser, ID, apiFetch, supabase } from "../api";
import { titleCase } from "../util";
import { toastErrorHandler, App, AppToaster } from "../App";
import { sha256 } from "js-sha256";
import { useNavigate, useParams } from "react-router-dom";

function Subtext(props: { children: React.ReactNode }) {
  return (
    <>
      <br />
      <span className={`${Classes.TEXT_MUTED} ${Classes.TEXT_SMALL}`}>{props.children}</span>
    </>
  );
}

export default function RegisterPage() {
  const { inviteId } = useParams() as { inviteId: ID<"i"> };
  const [session, setSession] = useState<Session | null>(null);
  const [inviteData, setInviteData] = useState<ApiInviteResponse | null>(null);
  const navigate = useNavigate();

  useEffect(() => {
    apiFetch<ApiInviteResponse>(`/invite/${inviteId}`)
      .then((ok) => {
        setInviteData(ok);
      })
      .catch((error) => {
        toastErrorHandler(error);
      });
    supabase.auth.getSession().then(({ data: { session } }) => {
      if (!session) {
        navigate(`/login/${inviteId}`);
      }
      setSession(session);
    });
  }, [inviteId, navigate]);

  const [avatarSelection, setAvatarSelection] = useState<string | undefined>();
  const [customName, setCustomName] = useState("");

  const setRadio = (setter: (value: string) => void) => (event: React.FormEvent<HTMLInputElement>) => {
    setter(event.currentTarget.value);
  };
  const submit = async () => {
    try {
      const token = await apiFetch<string>("/auth/supabase_register", "POST", {
        jwt: session!.access_token,
        avatar: avatarSelection!,
        name: customName,
        invite: inviteId,
      });
      localStorage.setItem("token", token);
      const user = await apiFetch<ApiUser>("/me");
      AppToaster.then((toaster) => toaster.show({ intent: Intent.SUCCESS, message: `Logged in as ${user.name}` }));
      window.localStorage.setItem("user", JSON.stringify(user));
      navigate("/home");
    } catch (error) {
      toastErrorHandler(error);
    }
  };

  let content = <NonIdealState icon={<Spinner size={SpinnerSize.LARGE} />} />;
  if (inviteData && session) {
    const identities = (session.user.identities ?? []).map((id) => ({
      name: id.provider,
      avatar: id.identity_data?.avatar_url as string | undefined,
    }));
    identities.push({
      name: "gravatar",
      avatar: session.user.email
        ? `https://www.gravatar.com/avatar/${sha256(session.user.email.trim().toLowerCase())}`
        : undefined,
    });
    content = (
      <div className="flex-center">
        <Callout intent={Intent.PRIMARY} className="margin-2-bottom">
          Since I don&apos;t want to deal with having to host user-uploaded images, you have to pick one. If you want a
          custom image, I recommend looking into <a href="https://gravatar.com/">Gravatar</a>.
        </Callout>
        <H3 className="thin-header fill-width text-center margin-auto">
          <b>{inviteData.user.name}</b> has invited you to <b>{inviteData.org.name}</b>
        </H3>
        <div className="pick-profile">
          <div>
            <H4>Pick your avatar</H4>
            <RadioGroup selectedValue={avatarSelection} onChange={setRadio(setAvatarSelection)}>
              {identities.map((id) => (
                <RadioCard key={id.name} value={id.name}>
                  <img src={id.avatar} width={48} height={48} />
                  <Subtext>{titleCase(id.name)} avatar</Subtext>
                </RadioCard>
              ))}
            </RadioGroup>
          </div>
          <div>
            <H4>Pick your username</H4>
            <InputGroup
              placeholder="Enter a custom name"
              className="fill-width"
              value={customName}
              onValueChange={(str) => setCustomName(str)}
            />
          </div>
        </div>
        <Button
          className="fit flex-center margin-auto"
          intent={Intent.SUCCESS}
          text="Submit"
          onClick={submit}
          disabled={!avatarSelection || customName === ""}
        />
      </div>
    );
  }

  return App(content);
}
